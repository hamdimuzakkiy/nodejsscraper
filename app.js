var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var promise = require('bluebird');
var fs = require('fs');

// get single html 
function getHTML(url, callback){		
	request(url, function(err, res, html){		
		callback(html);
	});
}

// get multiple html with parallel (using async.map)
function getHTMLs(urls, callback){	
	async.map(urls, function(url, callback){		
		getHTML(url, function(html){
			callback(null, html);
		})
	}, function(err,results){		
		callback(results);
	});
}

// get link child
function getLinkedCategory(html, classes, callback){
	if (html == null)
		html = '';	
	$ = cheerio.load(html);	
	result = $('ul').filter(classes).children().filter('li');
	callback(result);
}

// get url cateogry from root html and store to json
function getUrls(html, classes, callback){	
	getLinkedCategory(html, classes, function(result){
		var results = {};
		result.each(function(i,element){
			results[$(this).children().filter('a').text()] =
			$(this).children().filter('a').attr('href');
		});
		callback(results);
	});	
}

// get child url from each category
function getCategoryUrls(html, classes, callback){
	getLinkedCategory(html, classes, function(result){
		var results = [];		
		result.each(function(i, element){			
			results.push([$(this).children().filter('a').attr('href'),
			$(this).children().filter('a').children().filter('.valid-until').text()]);
		});
		callback(results);
	});
}

// get url all category with parallel (using async.map)
function getUrlDetails(htmls, callback){		
	async.map(htmls, function(html, callback){		
		getCategoryUrls(html, '.list2',function(result){
			callback(null,result);
		});
	}, function(err,results){
		callback(results);
	});
}

//clean text
function cleanText(lists){	
	for (i=0;i<lists.length;i++) {
		lists[i] = lists[i].split(/\s+/g).filter(function(val){
			if (val	== '')
				return false;
			return true;		
		}).join(' ');
	}		
	lists = lists.filter(function(val){
		if (val	== '')
			return false;
		return true;
	});
	return lists
}


// extract data from leaf html
function getAttributes(html, validate, callback){
	$		 				= cheerio.load(html);
	var title 				= $('div').filter('#merchant-detail').children().filter('h5').text();
	var image 				= $('div').filter('#banner').children().filter('img').attr('src');
	var logo 				= $('div').filter('#merchant-location').children()
							.children().filter('img').attr('src');
	var name 				= $('h5').filter('.orange').text();	
	var locationObj 		= $('div').filter('#merchant-location').children()
						  	.children().filter('p').first().text();
	var contact 		  	= $('div').filter('#merchant-location').children()
						  	.children().filter('p').last().text();
	var descriptionObj 	  	= $('div').filter('#merchant-detail')
						  	.children().filter('p').text();
	var linkObj		   	  	= $('div').filter('#merchant-detail')
						  	.children().filter('p').find('a');
	var imageObj		  	= $('div').filter('#merchant-detail')
						  	.children().filter('p').find('img');
	var description_link  	= [];
	var description_image 	= [];
	linkObj.each(function(i, element){
		description_link.push($(this).attr('href'));
	});
	imageObj.each(function(i,element){		
		description_image.push($(this).attr('src'));
	});
	var location 			= locationObj.split(/\n|\t/).filter(function(val){
		if (val	== '')
			return false;
		return true;		
	});;	
	var description_text 	= descriptionObj.split(/\n|\t/);		
	description_text 		= cleanText(description_text);	
	callback({	name			 :name, 
				title			 :title, 
				image			 :image, 
				validate 		 :validate,
				description 	 : {
					text 		 : description_text,
					link 		 : description_link,
					image 	     : description_image
				},				
				logo			 :logo, 
				location 		 :location,
				contact 		 : contact
	});	
}

// get detail each leaf
function getDetail(url, callback){	
	getHTML(url[0], function(html){		
		getAttributes(html, url[1],function(result){
			callback(result);
		});		
	});	
}

// get detail all leaf with parallel (using async.map)
function getDetails(urls, callback){	
	async.map(urls, function(urlsCategory, callback){
		async.map(urlsCategory, function(url, callback){			
			getDetail(url, function(detail){				
				callback(null,detail)
			});
		}, function(err, results){
			callback(null, results);
		});
	},function(err,results){
		callback(results);
	});
}

// write to file and return true if success otherwise false
function writeToFile(data, file, callback){
	fs.writeFile(file, data, function(err){			
		if (err || Object.keys(JSON.parse(data)).length == 0)
			return callback('Connection or Server Problem');
		return callback('Done');
	});
}

// main function (using async.waterfall)
console.log('starting ...');
function main(url, outputFile){
	async.waterfall([
		function(callback){
			console.log('getting root html');
			getHTML(url, function(html){				
				callback(null,html);
			});
		},
		function(html, callback){
			console.log('getting list category urls');
			getUrls(html, '.menu', function(urlEachCategory){				
				callback(null, urlEachCategory);
			});
		},
		function(urlEachCategory, callback){
			console.log('getting list category htmls');
			getHTMLs(urlEachCategory, function(htmlEachCategory){				
				callback(null, htmlEachCategory);
			});
		},
		function(htmlEachCategory, callback){		
			console.log('getting list urls each category');
			getUrlDetails(htmlEachCategory, function(urlsEachCategory){				
				callback(null, urlsEachCategory);
			});
		},
		function(urlsEachCategory, callback){
			console.log('get leaf html & extracting');
			getDetails(urlsEachCategory, function(result){				
				callback(null, result);
			});
		},
		function(result, callback){
			console.log('write to json');			
			writeToFile(JSON.stringify(result, null, 2), outputFile+'.json', function(status){				
				callback(null, status);
			});
		},
	], function(err, status){
		console.log('result : '+status);
	})	
}

// main(url, resultFileName)
main('http://m.bnizona.com/index.php/category/index/promo', 'result');